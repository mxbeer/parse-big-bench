package harschware.bigbench.drivers

import java.io.File
import scala.BigDecimal
import scala.collection.mutable.MutableList
import com.bizo.util.args4j.OptionsHelper.optionsOrExit
import com.bizo.util.args4j.OptionsWithHelp
import harschware.bigbench.utils.BigBenchLogUtils
import harschware.bigbench.utils.FileUtils
import harschware.bigbench.utils.DateUtils
import java.io.PrintWriter

import harschware.bigbench.ganglia._

/**
 * Driver class for the parse-big-bench project.
 *
 * @author Tim Harsch
 * @see https://github.com/intel-hadoop/Big-Bench
 */
object ParseBigBenchDriver {
  def main(args: Array[String]) {
    val options = optionsOrExit(args, new MyOptions)

    val jobReports: Map[String, List[AnyVal]] = BigBenchLogUtils.getJobReports(options.logPath)

    // find errors in logs and create a datastructure for looking up failed queries, so they can be
    //   zeroed out in output of runtimes.
    val filteredJobs = scala.collection.mutable.Map(jobReports.toSeq: _*)
    if (options.noErrors) {
      for (filename <- BigBenchLogUtils.getErrors(options.logPath.getAbsolutePath()).map(x => { x._1.getName })) {
        filteredJobs.put(filename, List(0, 0, 0, 0, 0, 0, 0))
      } // end for
    } // end if

    // sort the rows by labels
    val queryAndStream = """q([0-9][0-9])_([a-z]+)_(.*)_(\d+)\.log""".r
    val fileNames = filteredJobs.keys.toList;
    val sortedFilenames = fileNames.sortBy(fname => { val Some(parts) = queryAndStream.findFirstIn(fname); parts });

    val fileLabels = scala.collection.mutable.Map[String, String]();
    val pw = new PrintWriter(options.queryTimes)
    pw.println("Test Name\ttime(s)\tjobs\tmappers\treducers\tcumulative cpu(s)\treads (bytes)\twrites (byes)")
    for (fname <- sortedFilenames) {
      for (queryAndStream(query, engine, name, stream) <- queryAndStream.findAllIn(fname)) {
        val lbl = BigBenchLogUtils.testNameToShortLabel(name, stream, query, engine)
        fileLabels.put(fname, "q" + query)
        pw.print(lbl)
        pw.print("\t")
        pw.println(filteredJobs(fname).mkString("\t"))
      } // end for
    } // end for
    pw.close()

    // --gangliaNodes nodes.txt --gangliaChartTypes chartTypes.txt
    if (options.gangliaInput != null) {
      val startEndTimes = GangliaToolDataGenerator.getStartEndTimesFromCsv(options.logPath, fileLabels.toMap)
      val pwg = new PrintWriter(options.gangliaInput)
      val gangliaInputData = GangliaToolDataGenerator.genGangliaInputFile(startEndTimes, options.gangliaNodes, options.gangliaChartTypes)
      pwg.write(gangliaInputData)
      pwg.close()
    } // end if

  } // end main

} // end object

class MyOptions extends OptionsWithHelp {
  @org.kohsuke.args4j.Option(name = "--noErrors", usage = "if set this parameter will cause us to scan the logpath for errors and set failed queries to a completion time of zero")
  var noErrors: Boolean = false

  @org.kohsuke.args4j.Option(name = "--logPath", usage = "the log path of the BigBench run", required = true)
  var logPath: File = new File("/tmp")

  @org.kohsuke.args4j.Option(name = "--queryTimes", usage = "an absolute or relative filename for the TSV output of querytimes; defaults to \"queryTimes.tsv\"")
  var queryTimes: File = new File("queryTimes.tsv")

  @org.kohsuke.args4j.Option(name = "--gangliaInput", usage = "an absolute or relative filename where the ganglia-chart-tool input file will be generated.  IF not provided, no ganglia data will be generated regardless of the parameters gangliaNodes and gangliaChartTypes")
  var gangliaInput: File = null

  @org.kohsuke.args4j.Option(name = "--gangliaNodes", usage = "an absolute or relative filename that contains a node list to be used as an aid when generating the input file for ganglia-chart-tool.  If not provided, only charts for the cluster will be provided")
  var gangliaNodes: File = null

  @org.kohsuke.args4j.Option(name = "--gangliaChartTypes", usage = "an absolute or relative filename that contains a list of chart types to be used as an aid when generating the input file for ganglia-chart-tool.  If not provided the default types are load_all_report, mem_report, network_report.")
  var gangliaChartTypes: File = null
} // end class
