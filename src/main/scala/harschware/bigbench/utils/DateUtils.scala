package harschware.bigbench.utils

import scala.util.control.Breaks.break
import scala.util.control.Breaks.breakable

import org.joda.time.DateTime
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import org.joda.time.format.ISODateTimeFormat
import org.joda.time.format.PeriodFormatterBuilder
import org.slf4j.LoggerFactory

import com.github.nscala_time.time.Imports.forcePeriod
import com.github.nscala_time.time.Imports.richInt

object DateUtils {
  var LOGGER = LoggerFactory
    .getLogger(DateUtils.getClass());
  val DATE_FORMATTER_JODATIME_DEFAULT = ISODateTimeFormat.dateTimeParser().withOffsetParsed()
  val DATE_FORMATTER_GWEB = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm")
  // e.g. "Dec 07, 2014 11:25:36 PM"
  val DATE_FORMATTER_STD = DateTimeFormat.forPattern("MMM dd, yyyy h:mm:ss aa")
  val DATE_FORMATTER_ISO = ISODateTimeFormat.dateHourMinute()

  val FORMATTERS_DEFAULT = List(DATE_FORMATTER_JODATIME_DEFAULT, DATE_FORMATTER_GWEB, DATE_FORMATTER_STD, DATE_FORMATTER_ISO)

  val DATE_FORMATTER_JUSTTIME = DateTimeFormat.forPattern("HH:mm")
  val DATE_FORMATTER_JUSTTIME_WITH_SECONDS = DateTimeFormat.forPattern("HH:mm:ss")
  val DATE_FORMATTER_REALTIME_3 = DateTimeFormat.forPattern("HH'hmm'mss's")
  val DATE_FORMATTER_REALTIME_2 = DateTimeFormat.forPattern("mm'mss's")
  val DATE_FORMATTER_REALTIME_1 = DateTimeFormat.forPattern("ss's'")
  val FORMATTERS_JUSTTIME = List(DATE_FORMATTER_JUSTTIME, DATE_FORMATTER_JUSTTIME_WITH_SECONDS, DATE_FORMATTER_REALTIME_3, DATE_FORMATTER_REALTIME_2, DATE_FORMATTER_REALTIME_1)

  /**
   * takes a String and parses it using one or more formatters, returns the date as a jodatime DateTime
   */
  def parseDate(dateStr: String, formatters: List[DateTimeFormatter] = FORMATTERS_DEFAULT): DateTime = {
    var dt: DateTime = null;
    for (formatter <- formatters) {
      breakable {
        try {
          dt = formatter.parseDateTime(dateStr)
          break;
        } catch {
          case _: IllegalArgumentException => // try another format...
        } // end try/catch
      } // end breakable
    } // end for

    if (dt == null) {
      throw new IllegalArgumentException(s"No suitable date format found for ${dateStr}")
    } // end if

    return dt
  } // end function

  /**
   * takes a String and parses it using one or more formatters, returns the date as string in the given format
   */
  def printDate(dateStr: String, formatters: List[DateTimeFormatter] = FORMATTERS_DEFAULT, outputFormat: DateTimeFormatter = DATE_FORMATTER_ISO): String = {
    val dt = parseDate(dateStr, formatters)
    outputFormat.print(dt)
  } // end function

  private val realTimePeriodFormatter = new PeriodFormatterBuilder()
    .printZeroRarelyFirst()
    .appendYears()
    .appendSuffix("y", "y")
    .printZeroRarelyLast()
    .appendDays()
    .appendSuffix("d", "d")
    .printZeroRarelyLast()
    .appendHours()
    .appendSuffix("h", "h")
    .printZeroRarelyLast()
    .appendMinutes()
    .appendSuffix("m", "m")
    .printZeroRarelyLast()
    .appendSeconds()
    .appendSuffix("s", "s")
    .toFormatter();
  val longDurationPattern = """^(\d+)h:?(\d+)m:?(\d+)s:?(\d+)ms$""".r
  val shortDurationPattern = """^(\d+)h:?(\d+)m:?(\d+)s$""".r

  /**
   * parse strings such as "12y200d4h3m12s" and returns a jodatime Period
   */
  def parsePeriod(period: String): Period = {
    val db = 0.hours
    period match {
      case shortDurationPattern(hours, minutes, seconds) => {
        return db.plusHours(hours.toInt).
          plusMinutes(minutes.toInt).
          plusSeconds(seconds.toInt)
      } // end case
      case longDurationPattern(hours, minutes, seconds, millis) => {
        return db.plusHours(hours.toInt).
          plusMinutes(minutes.toInt).
          plusSeconds(seconds.toInt).
          plusMillis(millis.toInt)
      } // end case
      case _ => return db
    } // end match
  } // end function

  /**
   * parse strings such as "12y200d4h3m12s" and returns a normalized string
   */
  def printPeriod(period: String): String = {
    return realTimePeriodFormatter.print(parsePeriod(period))
  } // end function

  /**
   * given an  arbitrary jodatime period, prints a string such as 12y200d4h3m12s
   */
  def printPeriod(period: Period): String = {
    return realTimePeriodFormatter.print(period)
  } // end function
} // end object