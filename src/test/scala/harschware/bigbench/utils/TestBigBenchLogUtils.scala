package harschware.bigbench.utils

import java.io.File

import org.junit.Assert.assertTrue
import org.junit.Test
import org.slf4j.LoggerFactory

@Test
class TestBigBenchLogUtils {
  var LOGGER = LoggerFactory
    .getLogger(classOf[TestBigBenchLogUtils]);

  @Test
  def TestGetJobsStats() = {
    val logPath = getClass.getResource("/q24_hive_THROUGHPUT_TEST_SECOND_QUERY_RUN_IN_PROGRESS_0.log").getPath()
    val actual = BigBenchLogUtils.getJobStats(new File(logPath));

    println(actual)
    assertTrue(actual._1.equals(347));
  } // end test

  @Test
  def TestTimeParse() = {
    val secs = BigBenchLogUtils.timeParse("47.375s");
    assertTrue(secs == 47.375);

    val minAndSecs = BigBenchLogUtils.timeParse("5m47.375s");
    assertTrue(minAndSecs == 347.375);

    val HrAndMinAndSecs = BigBenchLogUtils.timeParse("10h5m47.375s");
    assertTrue(HrAndMinAndSecs == 36347.375);
  } // end test

} // end test class