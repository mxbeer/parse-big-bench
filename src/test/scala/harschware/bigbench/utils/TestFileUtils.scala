package harschware.bigbench.utils

import java.net.URL
import org.junit._
import org.junit.Assert._
import harschware.sandbox.JavaFileUtils
import org.slf4j.LoggerFactory
import java.io.File
import scala.io.Source
import scala.collection.mutable.MutableList

@Test
class TestFileUtils {
  var LOGGER = LoggerFactory
    .getLogger(classOf[TestFileUtils]);

  @Test
  def testTsv() = {
    val outputFile = new File("/tmp/filetest.txt")
    val sampleList = List(List("col1", "col2", "col3"), List(1, 2, 3))

    FileUtils.outputListToTsvFile(outputFile, sampleList);

    val actualContents = Source.fromFile(outputFile).mkString
    val expectedContents = io.Source.fromInputStream(getClass.getResourceAsStream("/TestTsv.txt")).mkString

    assertTrue(actualContents.equals(expectedContents));
  } // end test
  
} // end test class